import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ROUTING } from './app.routes';

import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SpinnerComponent } from './components/shared/spinner/spinner.component';
import { NotificationComponent } from './components/shared/notification/notification.component';
import { NotFoundComponent } from './components/errors/not-found/not-found.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { AuthenticationGuard } from './components/shared/guard/authentication.guard';
import { AuthService } from './services/auth/auth.service';
import { LayoutComponent } from './components/layout/layout.component';
import { SideNavigationComponent } from './components/dashboard/side-navigation/side-navigation.component';
import { TopNavigationComponent } from './components/dashboard/top-navigation/top-navigation.component';
import { UsersComponent } from './components/dashboard/users/users.component';

// main layout
import { NavigationComponent } from './main-layout/navigation/navigation.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { HomeComponent } from './main-layout/home/home.component';
import { ProfileComponent } from './main-layout/profile/profile.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SpinnerComponent,
    NotificationComponent,
    NotFoundComponent,
    LoginComponent,
    RegisterComponent,
    NavigationBarComponent,
    HomeComponent,
    LayoutComponent,
    SideNavigationComponent,
    TopNavigationComponent,
    ProfileComponent,
    UsersComponent,
    NavigationComponent,
    MainLayoutComponent,
    NavigationComponent
  ],
  imports: [
    BrowserModule,
    MDBBootstrapModule.forRoot(),
    ROUTING,
  ],
  providers: [
    AuthService,
    AuthenticationGuard,
  ],
  bootstrap: [AppComponent],
  schemas: [ NO_ERRORS_SCHEMA ],
})
export class AppModule { }
