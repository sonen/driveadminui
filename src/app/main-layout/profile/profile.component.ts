import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  @ViewChild('content') public contentModal;

  public name: string;

  constructor() { }

  profileFormModalName = new FormControl('', Validators.required);

  ngOnInit() {

  }

  show(value: string) {
      this.name = value;
      this.contentModal.show();
  }

}
