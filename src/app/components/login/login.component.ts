import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  actions = {
    'login': true,
    'register': false
  };

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  logIn() {
    this.router.navigate(['/dashboard']);
  }

  signUpView() {
    this.actions.login = false;
    this.actions.register = true;
  }

  logInView() {
    this.actions.login = true;
    this.actions.register = false;
  }

}
